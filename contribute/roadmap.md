---
title: Project Roadmap
description: Ideas, Stuff to-be-implemented and WIPs on the project,
published: true
date: 2020-06-16T10:02:06.680Z
tags: roadmaps
editor: markdown
---

# In the original code
See <https://github.com/thedevs-network/the-guard-bot/projects/1> and <https://github.com/thedevs-network/the-guard-bot/projects/2> for up-to-date boards!

# In Watchdog Chat Guard
> **Suggest more to get them added here!** Use our Issue Tracker in GitLab and there you go.
{.is-info}

## Ideas
| Title | Description | Related Issue/MR | Board Source |
| --- | --- | --- | ---
| N/A | N/A | N/A | N/A

## To be implemented soon
| Title | Description | Related Issue/MR | Board Source |
| --- | --- | --- | ---
| **Switch to webhooks** | Less CPU cycles + RAM, fast response, and no more downtimes in Glitch (for free users) | <https://github.com/thedevs-network/the-guard-bot/issues/118> | N/A

## Work-In-Progress
| Title | Description | Related Issue/MR | Board Source |
| --- | --- | --- | ---
| N/A | N/A | N/A | N/A
