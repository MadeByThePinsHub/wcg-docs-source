---
title: Setting up your Instance
description: Get started by cloning our Git repo or remixing in Glitch.
published: 1
date: 2020-05-21T10:06:05.765Z
tags: 
---

Before going any further, make sure your system (or your deployment location, like Glitch) meets all the [requirements](/install/requirements).

## Linux
1. Open your system's console.
2. Update your system dependencies.
```bash
## First, update our local index.
sudo apt update

## Then, upgrade everything. If you don't have Node.js installed, see the docs for details.
sudo apt -ygq upgrade
```

3. Check if your Node.js version installed on your machine is on 12.x or above.
```bash
node -v
## v12.14.1
```

4. Clone our Git repository first to get you started.
```bash
git clone https://gitlab.com/MadeByThePinsTeam-DevLabs/watchdog-chat-guard.git
cd ./watchdog-chat-guard
```

5. Install project dependencies with `npm install`. This should take minutes to install dependencies unless you using `pnpm`.

6. Configure your bot by editing the `config.js` file. See [the configration reference](/install/config) and [first-time setup](/user-guide/first-time-setup) for help.
```bash
## We're using Vim as a example, but you can use Visual Code Studio instead.
vi config.yml
```

7. Start the bot server with `npm start` and check if your bot is working by sending `/start` in PMs.

## macOS
1. Open the **Terminal** app.
2. Check your Node.js version first if it's 12.x or above.
```bash
node -v
## v12.14.1
```

3. Clone our Git repository first to get you started.
```bash
git clone https://gitlab.com/MadeByThePinsTeam-DevLabs/watchdog-chat-guard.git
```

4.  Install project dependencies with `npm install`. This should take minutes to install dependencies unless you using `pnpm`.

5. Configure your bot by editing the `config.js` file. See [the configration reference](/install/config) and [first-time setup](/user-guide/first-time-setup) for help.

6. Start the bot server with `npm start` and check if your bot is working by sending `/start` in PMs.

## Docker
### Prebuilds
*Don't spoil, coming soon.*

### Reproducible Builds
1. Install the Docker CE  for Linux or Docker Desktop for Windows 10/macOS.

2. Follow step 4 and 6 from instructions for Linux instances.

3. Build the image with `docker build -t watchdog-chat-guard .` Then check if

4. Start up your containner.
```bash
## You can do this if your secrets are on your console vars.
docker run -v $PWD/.data:/app/.data --rm -itd 3000:3000 watchdog-chat-guard -e SPAMWATCH_API_HOST=https://api.spamwat.ch -e SPAMWATCH_API_TOKEN=ReplaceWithYourAPITokenSitiSchuGeneratedForYou -e TELEGRAM_API_TOKEN=ReplaceWithYourBotTokenHer
```

## Kubernetes
> :warning: **Using Watchdog Chat Guard in Kubernetes is experimental!** While the developers of the Guard bot added support for Dockerinized images, using the Watchdog Chat Guard in Kubernetes is not yet tested.{.is-warning}

### Using the Helm Chart
*Don't spoil, we'll build that soon.*

### From the Docker image
> :information-center: Make sure you
{.is-info}


1. Follow the setup instructions for installing the Docker image.
