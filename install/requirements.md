---
title: Requirements
description: Prerequisites to install Watchdog Chat Guard
published: 1
date: 2020-05-21T10:06:01.460Z
tags: 
---

# Server Requirements
This means it runs on **Linux, macOS, Windows** as well as container solutions such as **Docker / Kubernetes**.[^1]

## CPU Cores
Wiki.js runs perfectly fine on a single CPU core. However, **2 cores or more are recommended** if you deploying your bot to production.

## RAM
Linux machines usually required to have **at least 1 GB of RAM**. Windows and mcOS mchines usually required a little more RAM.

While the process itself usually sits at around 102MB of RAM, some events (such as DDoS attacks on your API endpoints or even processing tons of crap) result in short bursts in RAM usage.
## Storage
Storage is required for storing flatfile databases in the `.data` directory (in compliant to Glitch ones). Also, make sure that your storage solutions support presistence.[^1]

## Internet Access
Internet access is required to interact with the Telegram Bot API, install and update your instance and its dependencies.

# Domain
Watchdog Chat Guard currently not require to have a domain because the Guard only supports long polling at this time, unless you want to check if your bot server is up or writing some API support.

# Database
The Chat Guard doesn't need a database yet, see `Storage` section. Stay tuned!

# Node.js
Node.js **12.x** or later is required. If we found that you're using Node.js below what's supported, the bot server will sends you a error in the logs and then exits with a non-zero code.

[^1]: Don't try to deploy the app in Heroku as local storage presistence is not built-in by default.