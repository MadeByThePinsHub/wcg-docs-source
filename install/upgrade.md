---
title: Upgrading your Instance
description: As soon as our code updates had been arrived, consider upgrading yours.
published: true
date: 2020-10-16T12:25:55.937Z
tags: setup, upgrade, user guide
editor: markdown
---

Because you're using Git as a way to deliver updates to our codebase, you need to update your instance manually.

> **Make your bot server offline first!** It's dangerous to update your instance's code while running the server. To take your server down, send a `SIGTERM` signal and it'll safely exits.
>
> We don't promise that you'll get back into work after updating the instance during your server is active.
{.is-warning}

> **Back up your `.data` directory first!** We don't know what's next after updating your instance.
{.is-info}

> **Because our repository doesn't have stable branch yet, expect to fire up some errors.** If you experience any errors, [create a new GitLab issue](https://gitlab.com/MadeByThePinsTeam-DevLabs/watchdog-chat-guard/issues/new).
{.is-warning}


## With Git
> **We recommend not to edit the source code, unless you know what you're doing.** If you forked the source code, please make sure you resolve any conflicts as possible.
{.is-info}

1. Open the console and check for updates by fetching commits first with `git fetch --all`.
2. Then do `git status` if you remixed our codebase.
3. After getting the status, trigger `git pull origin develop` in your console and wait to merge stuff.
4. When Git found merge conflicts, fix them as quickly as possible. If you're new to resolving conflicts, remember the `<<<`, `===` and `>>>` everywhere. To use yours, remove what's below the `===`. If you want to use ours, remove what's above the `===`. Want to merge both? You need to do some code as you merge both.
5. Commit changes, and optionally, push to your fork's repository.
6. Return to the work with `npm start`.

## With Docker / Podman

> If you're using Podman, replace `docker` with `podman`.
{.is-info}

1. Stop the containner with `docker stop watchdog-chat-guard`.[^1]
2. Repeat step 1 to 5 on `With Git` method.
3. Re-build your local image with newest commits with `docker build -t watchdog-chat-guard .`.
4. Finally, restart with the same command used when you installed your instance for the first time.[^2]

[^1]: Do not delete your image including its volume with `docker rm -v watchdog-chat-guard` to avoid data loss.
[^2]: [See here for details](/install/setup).

## Updating from upstream repository
> **Proceed with caution!** It's not recommended to pull changes straight from the upstream repo because we'll try to update our codebase as soon as possible.
{.is-warning}


1. Open your console and type `git remote -v` to check if the `upstream` is defined.
2. If not defined, add it as a remote and fetch updates.

```bash
# Add the upstream repo to the list of remotes
git remote add upstream https://github.com/TheDevs-Network/the-guard-bot.git

# Then fetch.
git fetch upstream # Optionally, use the --all flag to get updates from all remotes.
```
3. Run `git pull upstream master` and fix remaining conflicts.
4. **(optionally)** Commit the resulting merge and push to your fork.