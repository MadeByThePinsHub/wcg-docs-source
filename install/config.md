---
title: Configuration Reference
description: 
published: 1
date: 2020-05-21T10:05:56.363Z
tags: 
---

A config file, named `config.js`, must be located at the root of your instance.

> **Note**: If your instance is from the original source code, you must rename the file `config.sample.js` to `config.js`.
{.is-info}

Listed below are all possible options that can be changed in your `config.js` file without hard-coding them.

# Basics and Essientals
Atleast 2 of these are required to run your instance of Watchdog Chat Guard.
## Bot masters / `master`
**Type**: Number (for UIDs) or string (for usernames, but we don't recommend it)

**Description**: Bot master/s are the people who can promote/demote admins and add to groups.

## Telegram Bot API token / `token`
**Type**: Environment variable from `TELEGRAM_API_TOKEN`

**Description**: Telegram Bot API token, generated from [BotFather](https://t.me/BotFather).

# Transparency and Logging
## Logging Chats / `chats`
### Presence Logging Chat / `chats.presenceLog`
**Type**: Number (for chat ID) or Boolean (`false` only)

### Report Notifications Chat / `chats.reports`
**Type**: Number (for chat ID) or Boolean (`false` only)

**Description**: Chat to send report notifications when used `/report` command from ordinary users.

# Customization

# Clearing Service

> Even the clearing service options are defined, network admins can visit **Recent Actions** to see what the bot is doing.
{.is-info}


## Delete commands after some time / `deleteCommands`
**Type**: Option-based string: Everything (`all`), commands for the bot (`own`), or nothing (`none`)

**Description**: When defined, the bot will remove commands sent by users depending on what type of commands should be cleared.

## Delete notes after some time / `deleteCustom`
**Type**: Configuration group

**Description**: Configuration for the clearing service on removing custom commands from the chat.

### Command names longer than Z characters / `deleteCustom.longerThan`
**Type**: Number (only UTF-16 characters are counted)

**Description**:

### Delete custom commands after some time / `deleteCustom.after`
**Type**: String to be parsed by `millisecond` Node module.

**Description**: When defined, timeout before custom commands to get nuked from the chat history. To immediately nuke then, pass it with `0`, but be careful!

## Delete joins after some time / `deleteJoinsfter`
**Type**: String to be parsed by `millisecond` Node module.

**Description**: When defined, timeout before removing `X joined the group` and `Y left the group` service notifications. To immediately nuke them, pass it with `0`.

# Integrations
We'll update thi list as more integrations are coming to the original source code.
## SpamWatch API Integration / `spamwatch`
**Type**: Configuration group or Boolean `false` to disable the integration.

**Description**: Configuration for the SpamWatch API integration.

**How to disable the integration**:
```js
'use strict';
// Blah blah blah
// Blah blah blah
// Blah blah blah

const config {

	// Blah blah blah

	// Either use false...
	spamwatch: false
  
  // ...or comment out to disable the integration
  // spamwatch: {...}
  
  // Blah blah blah
  
 }
```

### SpamWatch API Token / `spamwatch.token`
**Type**: String (for token)

**Description**: API token for SpamWatch API, this config is only required to enable the integration to `api.spamwat.ch`.

### SpamWatch API Host / `spamwatch.host`
**Type**: String (for API host)

**If undefined, falls back to**: `https://api.spamwat.ch`

**Description**: A valid hostname to a SpamWatch API instance, usually used if you're been firewalled or just need a proxy. Extras like `/` or even like `/api/spamwatch` are not allowed.

# Sample config file
This is how the Pins team's instance config file looks like, including comments:[^1]

```js
// @ts-check
"use strict";

/**
 * @typedef { import('./typings/config').Config } Config
 * @typedef { import('./typings/config').InlineKeyboard } InlineKeyboard
 */

/*
 * If you remix me, pleae edit this file instead
 * of starting from strach.
 *
 * Config file in JSON format (`config.json`) is also supported.
 * For backwards compatibility, and because why not, it needs no extra code.
 */

/**
 * Millisecond
 * String to be parsed by https://npmjs.com/millisecond,
 * or number of milliseconds. Pass 0 to remove immediately.
 * @typedef {( number | string )} ms
 */

/**
 * @type {Config}
 */
const config = {
  /**
   * @type {!( number | string | (number|string)[] )}
   * ID (number) or username (string) of master,
   * the person who can promote and demote admins,
   * and add the bot to groups.
   *
   * Edit this one with your own values.
   * And we recommend to use IDs instead.
   */
  master: [705519392, 709590349],

  /**
   * @type {!string}
   * Telegram Bot token obtained from https://t.me/BotFather.
   *
   * You should pull your bot token from env, instead of pasting it here.
   * That's bad for your bot and your groups!
   */
  token: process.env.TELEGRAM_API_TOKEN,

  chats: {
    /**
     * @type {(number | false)}
     * Chat to send member join/leave notifications to.
     * Pass false to disable this feature.
     */
    presenceLog: -1001273802813,

    /**
     * @type {(number | false)}
     * Chat to send report notifications to.
     * Pass false to disable this feature.
     */
    report: -1001273802813
  },

  /**
   * @type {( 'all' | 'own' | 'none' )}
   * Which messages with commands should be deleted?
   * Defaults to 'own' -- don't delete commands meant for other bots.
   */
  deleteCommands: "own",

  deleteCustom: {
    longerThan: 450, // UTF-16 characters
    after: "5 minutes"
  },

  /**
   * @type {(ms | false)} Millisecond
   * Timeout before removing join and leave messages.
   * [Look at typedef above for details.]
   * Pass false to disable this feature.
   */
  deleteJoinsAfter: "2 minutes",

  /**
   * @type {(ms | { auto: (ms | false), manual: (ms | false) } | false)}
   * Timeout before removing auto-warn messages.
   * [Look at typedef above for details.]
   * Pass an object with { auto, manual } for more granular control
   * over which messages get deleted
   * Pass false to disable this feature.
   */
  deleteWarnsAfter: false,

  /**
   * @type {(ms | false)}
   * Timeout before removing ban messages.
   * [Look at typedef above for details.]
   * Pass false to disable this feature.
   */
  deleteBansAfter: false,

  /**
   * @type {string[]}
   * List of blacklisted domains.
   * Messages containing blacklisted domains will automatically be warned.
   * If the link is shortened, an attempt will be made to resolve it.
   * If resolved link is blacklisted, it will be warned for.
   */
  blacklistedDomains: ["gab.com", "tinyurl.com"],

  /**
   * @type {( string[] | false )}
   * List of whitelisted links and usernames,
   * For channels and groups to stop warning users for them.
   * Pass false to whitelist all links and channels.
   */
  excludeLinks: [
    "BestOfSawada",
    "TheDevs",
    "RecapTime",
    "ThePinsTeam_TransparencyHub",
    "ThePinsTeam_FedLogs",
    "Telegram",
    "DevChangeLogs_byMPTeam",
    "UpdatesFromThePinsTeam",
    "Kuys_Potpot",
    "SpamWatch",
    "SpamWatchFederationLogs",
    "madebythepins.tk",
    "handbooksbythepins.gq",
    "recaptime.tk"
  ],

  /**
   * @type {ms}
   * Don't count warns older than this value towards automatic ban.
   * [Look at typedef above for details.]
   */
  expireWarnsAfter: Infinity,

  /**
   * @type {InlineKeyboard}
   * Inline keyboard to be added to reply to /groups.
   * We use it to display button opening our webpage.
   */
  groupsInlineKeyboard: [],

  numberOfWarnsToBan: 5,

  /**
   * @type {string[]}
   * List of plugin names to be loaded.
   * See Readme in plugins directory for more details.
   */
  plugins: ["codeblockChecker"],

  spamwatch: {
    /**
     * @type {(string | false )}
     *
     * SpamWatch API token.
     * To get a fresh token, join SpamWatch Support in Telegram.
     *
     * You should pull your API token from env, not here!
     * Pasting it here is a bad pratice.
     */
    token: process.env.SPAMWATCH_API_TOKEN,

    /**
     * @type {string}
     * SpamWatch API host. Defaults to 'https://api.spamwat.ch'.
     * Unless you have issues while connecting there, keep it unchanged.
     *
     */
    host: process.env.SPAMWATCH_API_HOST
  },

  /**
   * @type {InlineKeyboard}
   * Inline keyboard to be added to warn message.
   * We use it to display button showing our rules.
   */
  warnInlineKeyboard: [[ "Read the rules", "https://t.me/ThePinsTeam_GeneralChatRules" ]]
};

module.exports = Object.freeze(config);
```

[^1]: Copied from commit `db0afde4883ca8e61d0afb68c57049e020758444`, see latest updates [here](https://gitlab.com/MadeByThePinsTeam-DevLabs/watchdog-chat-guard/-/blob/develop/config.js).