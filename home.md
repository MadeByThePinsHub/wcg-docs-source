---
title: Home
description: The main page of the documentation.
published: true
date: 2020-06-14T12:14:02.043Z
tags: 
editor: markdown
---

> **Work in progress.** Some stuff maybe incomplete or outdated. We're looking for contributions to the documenttion soon.
{.is-warning}

## Getting Started
- [Requirements *Make sure that you deploy it somewhere that supports presistance in local storage. If you're using Heroku, we don't recommend it.*](/install/requirements)
- [Installing Watchdog Chat Guard *It just takes minutes to get you running.*](/install/setup)
- [Updating your instance *Just `git pull` stuff and you're ready.*](/install/upgrade)
- [Configuration Reference *Need help in configuring your bot? We can help!*](/install/config)
{.links-list}

## User Guide
- [First Time Configuration *Set-up your bot first before you hit start.*](/user-guide/first-time-use)
- [Commands Reference *Some of these are also found in Rose, among others.*](/user-guide/commands)
- [Adding to groups *Only bot masters can do this.*](/user-guide/add-to-groups)
- [Custom Commands/Notes *Notes are accessible by using exclaimation points, not forward-slashes and not even hashtags (like in Rose).*](/user-guide/notes)
- [Using Plugins *Some plugins are included with the Watchdog Chat Guard and you can even create your own.*](/user-guide/plugins)
{.links-list}

## Integrations
- [Integration Support Roadmap *We'll update once there's new integrations onboarding to the code.*](integrations/roadmap)
- [SpamWatch *Watchdog Chat Guard got first-class support for SpamWatch API.*](/integrations/SpamWatch)
{.links-list}
- [Sibyl System *Coming soon, as Sawada said in One Punch Support.*](/integrations/SibylSystem)

## Getting Support
- [The Guard Support *As Watchdog Chat Guard is a fork of The Devs Netowrk's The Guard, you can get support straight from developers at the Devs Network.*](https://t.me/TheGuardSupport)
- [Watchdog Chat Guard Support](https://t.me/WatchdogChatGuardSupport)
{.links-list}

## About & Contribute
- [Bug Reporting *Found a bug? A security issue? Help us catch and fix it!*](/contribute/report-bugs)
- [Documentation *The documentation is currently and still Work-In-Progress.*](/contribute/docs)
- [Project Roadmap *We made our roadmap for WCG open for people to contribute.*](/contribute/roadmap)
{.links-list}

## Copyright Notice
`The Guard` icon is from [Entypo+](http://entypo.com/) by by Daniel Bruce, licensed under CC BY-SA 3.0.