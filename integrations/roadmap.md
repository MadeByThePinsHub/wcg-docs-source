---
title: Integration Roadmap
description: An roadmap for third-party integrations the WCG supports it.
published: true
date: 2020-06-14T12:29:40.221Z
tags: 
editor: markdown
---

# Roadmap Table
| Integration | Description | Who Made This API? | Status | Type | Related MR/Issue | Integration Version[^1] |
| ----- | ----- | ----- | ----- | ----- | ----- | ----- |
| [SpamWatch](/integrations/spamwatch) | An antispam service for Telegram with 100% human support. | [Simon  Schürrle / @SitiSchu](https://t.me/SititSchu) | :white_check_mark: | API Wrapper/Built-in Integration | [Original Code's Issue #15](https://github.com/thedevs-network/the-guard-bot/issues/15) | `0.1.0` |
| [SpamBlockers](/integrations/spamblockers) | | [Andrei Jiroh / @AndreiJirohHaliliDev2006](https://gitlab.com/AndreiJirohHaliliDev2006) | :construction: | API Wrapper/Built-in Integration | - | `0.1.0` |
| Sibyl System | To Be Updated / <https://t.me/SibylSystem/101> | - | :question: | - | - | - |
| DEAI / Dauha Engine | AI-powered antispam service for Telegram, started as a fork of the Guard, like WCG. | [@Felenov](https://t.me/Felenov) | :question: | - | - | -

## Legends in `Status` column
:x: - **Removed**: Not available from the codebase, removed after some period of time being deprecated.
:warning: - **Deprecated**: Still available, but no longer supported.
:white_check_mark: - **Currently Supported**: Available and currently supported.
:construction: - **Work In Progress**: Only proceed with caution, as some features may break. Either they're integrated with WCG but under experiment or the API is underway.
:question: - **Coming Soon**: Stay tuned for updates, as integration authors make their way to integrate with the bot.

# Want to add more integraions?
- [**Suggest a new integration here**](https://gitlab.com/MadeByThePinsTeam-DevLabs/watchdog-chat-guard). Once we'll received it, we'll review it first, then either forward it to friends at the Devs Network for review or we'll add it to [our roadmap](/contribute/roadmap) and you can even send us a MR related to that.
- [**Write a new plugin and send us a MR**](/dev/plugins). See the developers' page linked to learn more about our policies on writing plugins if you want to make it to our bot.

[^1]: The integration version must be based on the API version, not on the version of their API wrapper for Node.js.