---
title: Commands Reference
description: 
published: true
date: 2020-06-14T12:26:41.974Z
tags: 
editor: markdown
---

Depressed on how to use the built-in commands shipped on the bot?

# Bot Masters only
> **Check your configurtion if you correctly define bot masters.** If your instance doesn't have any bot operators/masters, these commands will not work.
{.is-danger}

## Managing network admins
**To promote a new admin**: use `/promote {user}` and it'll automaticlly promote that user to an admin with limited permissions. Once promoted, admins can [use every commands an Network Admin can use](#network-admins).

**To demote a admin**: use `/demote {user}` and it'll automatically remove user's admin permissions. This will lose access to admin commands, and cannot be protected against warns and bans.

## Managing connected chats
**To leave a chat cleanly**: use `/leave {group}` and will sliently leaves the target chat. Please be reminded that banned users must manually unbanned.

**To hide or show groups from showing in `/groups`**: use either `/showgroup {chat}` or `/hidegroup {chat}`. Useful if you want to hide specific groups where [CalsiBot](https://t.me/CalsiBot) is installed.

# Network Admins Only
> **Commands below are also work with bot operators.**
{.is-info}


# Commands for Everyone
## Network Admins and Groups
**To pull the network admins**: use `/staff` and will magically pull the list of network admins from DB.

**To check where groups the bot is active**: use `/groups` and will show all the groups available to view. If network admins hide some groups, it'll not available for you, even network admins. (Only the bot operators knows!)

## Flagging
**To report a message in the group**: reply `/report` to the target message and will notified the network admins. If a network admin was got reported, the bot will send an error (`Cannot flag a Network Admin, please contact bot operators for help.`) for any attempts.

# Legends
| Variable | Description 
| ---- | ---- 
| `{user}` | Placeholder for a user. Can be the ID, username or by reply.
| `{chat}` | Placeholder for a chat. Can be the ID or its name.
