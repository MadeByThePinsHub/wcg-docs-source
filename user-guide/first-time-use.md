---
title: First-time Setup
description: 
published: 1
date: 2020-05-21T10:06:27.860Z
tags: 
---

Setting up your bot is easy as you might think and usually take minutes to get you running.

## Configuring your instance
> :warning::closed_lock_with_key: **Don't commit your secrets!** We handle API keys and other config secrets using the environment variables configured on your machine-side.
{.is-danger}

The most important configuration keys you need are the Telegram Bot API token and the bot masters. The rest are optional and can be configured later at any point of time.

**From the configuration reference**: [The Bot Master config syntax](/install/config/#bot-masters-master) and [Telegram Bot API token config syntax](/install/config/#telegram-bot-api-token-token)

## Adding to groups
> :warning: **Moving between hosts or machines?** Make sure you backed up everything in the `.data` directory before you start your bot in the new place. Otherwise, you need to start over again.
{.is-warning}

> :warning: **Confirm that you had configured to recongize your bot that you're the master.** Check if `config.master` has been configured correctly, otherwise your instance will complain that you're not the bot master and then leave the group. See the [configuration reference](/install/config) for details about configuring Bot Masters.
{.is-warning}


**If you are the bot master**: Copy your instance's username, open your group you administer then open **Group Info** -> **Manage group** page -> **Administrators** -> **Add group**, paste your instance's username in the search bar and tap it. Make sure you grant your instance all of the permissions (excluding the **Add Users/Invite Users via Link** if you want to hide your group

**If you are just a network admin**: Ask your bot master and try to add it to the group you specified using the instructions above.

## Adding network admins
> :information_source: **Don't let your bot get rate-limited after adding to groups!** Let allow your bot to get some time getting your group members' data such as user IDs and usernames to its local DB files in the `.data` directory. This will lessen your bot's dependence on the API and to minimize the chances of getting rate-limited.
>
> While this might take some time for first time to populate the `.data/User.db` file, any subsequent actions such as bans and warns should make your bot robust.
{.is-info}


When your bot has have some user metadata on cache, you can use the `/promote` command. To use it, reply to a message your target sent and wait for the success prompt.

## Moderating users
> :x: **You can't ban users in PMs.** To ban users, use it in a group for transparency reasons. We're been working for a way to send bans and unbans in a log chat soon as the original code supports it.
{.is-danger}

> :warning: **If the user you want to ban wasn't in your bot's local DB, use its ID instead.**
{.is-warning}

In groups where your instance is installed:
- ban users with `/ban <userid|username|reply> <reason>` and they'll banned across your Community Network.
- you can also warn them with `/warn <userid|username|reply> <reason>`
- 